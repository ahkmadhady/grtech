
@extends('layouts.app') 
@section('content')
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Companies</h1>
          </div> 
           
        </div> 
      </div> 
    </div> 
    
    <section class="content">
      <div class="container-fluid"> 
      @if (session('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="card"> 
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="card-title">List Companies</h3>
                                </div>
                                <div class="col-md-6 d-flex flex-row-reverse">
                                    <div class="btn-group" role="group"> 
                                        <a href="{{route('companies')}}" class="btn btn-sm btn-outline-primary" >  <i class="fas fa-sync"></i> Refresh </a>
                                        <a href="#" class="btn btn-sm btn-outline-primary" onclick="showModal()">  <i class="fas fa-plus-circle"></i> Add Companies </a>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                        <div class="card-body">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Logo</th>
                                    <th>Website</th> 
                                    <th>Action</th> 

                                </tr>
                                </thead>
                                <tbody>
                                 
                                </tbody>
                            </table>
                        </div>  
                    </div>
                </div> 
            </div>  
        </div>   
    </section>  
    <!-- modal companies -->
    <div class="modal fade" id="modal-companies">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="title_modal">Form Add Companies</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="" class="form" id="form_companies" enctype="multipart/form-data">
                    @csrf  
                <div class="row">
                    <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name" id="name">
                                <input type="hidden" class="form-control" name="id_companies" id="id_companies">
                                <span id="er_name" class="text-danger"></span>
                            </div>
                    </div> 
                    <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" class="form-control" name="email" id="email">
                                <span id="er_email" class="text-danger"></span>
                            </div>
                    </div> 
                    <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Website</label>
                                <input type="text" class="form-control" name="website" id="website">
                                <span id="er_website" class="text-danger"></span>
                            </div>
                    </div> 
                    <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Logo</label>
                                <input type="file" class="form-control" name="logo" id="logo">
                                <span id="er_logo" class="text-danger"></span>
                            </div>
                    </div> 
                </div>
               
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" id="save_companies"> <i class="fas fa-save"></i> Save Companies</button>
            </div>
            </form>
          </div> 
        </div> 
    </div>

    <!-- modal delete -->
    <div class="modal fade" id="modal_delete">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="title_modal">Confirmation</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                  <p>Do you want to delete company data <span style="font-weight: bold;" id="nama_company"></span></p>
                  <input type="hidden" class="form-control" id="id_delete">
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" id="save_companies" onclick="deleteCompany()"> <i class="fas fa-save"></i> Delete Company</button>
            </div>
            
          </div> 
        </div> 
    </div>

    <!-- modal logo -->
    <div class="modal fade" id="modal_logo">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="title_modal">Logo Company</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" id="data_logo" style="text-align: center;">
                <!-- conten -->
            </div>
            <div class="modal-footer justify-content-between">
             
            </div> 
          </div> 
        </div> 
    </div>
@endsection
@push('script')   
<script>
    $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('companies') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'}, 
            {data: 'email', email: 'email'},
            {data: 'logo', email: 'logo',
              "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                  $(nTd).html('<a href="javascript:void(0)" onclick="logoCompany(`'+oData.id+'`)">'+oData.logo+'</a>');
              }
            },
           
            {data: 'website', name: 'website'},
            {data: 'Action', name: 'Action', orderable: false, searchable: false},
        ]
    });
  });
</script>
<script src="{{asset('js/companies.js')}}"></script>
 
@endpush