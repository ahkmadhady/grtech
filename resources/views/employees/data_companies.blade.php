<table style="width: 100%;">
    <tr>
        <td colspan="3" style="text-align: center;">
        <img src="{{asset('logo')}}/{{$companies->logo}}" id="logo_company" alt="Logo" style="
                        border: 1px solid #ddd;
                        border-radius: 4px;
                        padding: 5px;
                        width: 13%;
                        height: 8%"/>
        </td>
    </tr>
    <tr>
        <td>Company Name</td>
        <td>:</td>
        <td>{{$companies->name}}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>:</td>
        <td>{{$companies->email}}</td>
    </tr>
    <tr>
        <td>Website</td>
        <td>:</td>
        <td>{{$companies->website}}</td>
    </tr>
</table>