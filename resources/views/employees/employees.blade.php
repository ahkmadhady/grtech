
@extends('layouts.app') 
@section('content')
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Employees</h1>
          </div> 
           
        </div> 
      </div> 
    </div> 
    @if (session('error'))
     <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('error') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
             <span aria-hidden="true">&times;</span>
         </button>
    </div>
    @endif
    <section class="content">
      <div class="container-fluid"> 
      @if (session('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="card"> 
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="card-title">List Employees</h3>
                                </div>
                                <div class="col-md-6 d-flex flex-row-reverse">
                                    <div class="btn-group" role="group"> 
                                        <a href="{{route('employees')}}" class="btn btn-sm btn-outline-primary">  <i class="fas fa-sync"></i> Refresh </a>
                                        <a href="#" class="btn btn-sm btn-outline-primary" onclick="showModal()">  <i class="fas fa-plus-circle"></i> Add Employee </a>
                                    </div>
                                </div>
                            </div> 
                        </div>  
                        <div class="card-body">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th>Company</th>
                                    <th>Email</th>
                                    <th>Phone</th> 
                                    <th>Action</th> 

                                </tr>
                                </thead>
                                <tbody>
                                
                                </tbody>
                            </table>
                        </div>  
                    </div>
                </div> 
            </div>  
        </div>   
    </section>  
    <!-- modal companies -->
    <div class="modal fade" id="modal_employee">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="title_modal">Form Add Employee</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">First Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="first_name" id="first_name">
                            <input type="hidden" class="form-control" name="id_employee" id="id_employee">
                            <span id="er_first_name" class="text-danger"></span>
                        </div>
                    </div> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Last Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="last_name" id="last_name">
                            <span id="er_last_name" class="text-danger"></span>
                        </div>
                    </div> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Company</label> 
                            <select name="company" id="company" class="form-control">
                                <option value=""></option>
                                @foreach ($companies as $datas)
                                    <option value="{{$datas->name}}">{{$datas->name}}</option>
                                @endforeach
                            </select>
                            <span id="er_company" class="text-danger"></span>
                        </div>
                    </div> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" class="form-control" name="email" id="email">
                            <span id="er_email" class="text-danger"></span>
                        </div>
                    </div> 
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Phone</label>
                            <input type="number" class="form-control" name="phone" id="phone">
                            <span id="er_phone" class="text-danger"></span>
                        </div>
                    </div> 
                </div>
               
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="actionSubmit()"> <i class="fas fa-save"></i> Save Employee</button>
            </div> 
          </div> 
        </div> 
    </div>

    <!-- modal delete -->
    <div class="modal fade" id="modal_delete">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="title_modal">Confirmation</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                  <p>Do you want to delete employee data <span style="font-weight: bold;" id="nama_employee"></span></p>
                  <input type="hidden" class="form-control" id="id_delete">
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="deleteEmployee()"> <i class="fas fa-save"></i> Delete Employee</button>
            </div>
            </form>
          </div> 
        </div> 
    </div>

    <!-- modal company data -->
    <div class="modal fade" id="modal_company">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="title_modal">Data Company</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" id="datas_company">
                <!-- content -->
            </div>
            <div class="modal-footer justify-content-between"> 

            </div> 
          </div> 
        </div> 
    </div>
@endsection
@push('script')    
<script>
    $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('employees') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'full_name', name: 'full_name'},
            {data: 'company', name: 'company',
              "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                  $(nTd).html('<a href="javascript:void(0)" onclick="showCompany(`'+oData.company+'`)">'+oData.company+'</a>');
              }
            },
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'Action', name: 'Action', orderable: false, searchable: false},
        ]
    }); 
  }); 
</script>
<script src="{{asset('js/employees.js')}}"></script>
@endpush