<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'email' => 'admin@grtech.com.my',
                'password' => Hash::make('password'),
                'level' => "ADMIN"
            ],

            [
                'name' => 'User',
                'email' => 'user@grtech.com.my',
                'password' => \Hash::make('password'),
                'isAdmin' => "USER"
            ]
        ]);
    }
}
