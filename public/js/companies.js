

 $(document).ready(function () {
    //  save companies
    $("#save_companies").click(function (event) {
        event.preventDefault();
        var form = $("#form_companies")[0];
        var data = new FormData(form);
        data.append("CustomField", "This is some extra data, testing");

                $.ajax({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                }, 
                type: "POST",
                enctype: "multipart/form-data",
                url: "companies/save-companies",
                data: data,
                processData: false,
                contentType: false,
                cache: false, 
               
                success: function (data) {
                    if (data.errors) { 

                        if (data.errors.name) {
                            $("#er_name").html(data.errors.name);
                        }else{
                            $("#er_name").html("");
                        }

                        if (data.errors.email) {
                            $("#er_email").html(data.errors.email);
                        }else{
                            $("#er_email").html("");
                        }

                        if (data.errors.website) {
                            $("#er_website").html(data.errors.website);
                        }else{
                            $("#er_website").html("");
                        }

                        if (data.errors.logo) {
                            $("#er_logo").html(data.errors.logo);
                        }else{
                            $("#er_logo").html("");
                        } 

                    } else {
                    
                        $("#er_name").html("");
                        $("#er_email").html("");
                        $("#er_website").html("");
                        $("#er_logo").html(""); 
                        location.reload(true);
                    }
                },
            });
        });

        // clear error name
        $("#name").change(function () {
             $("#er_name").html("");
        });

         // clear error email
        $("#email").change(function () {
            $("#er_email").html("");
        });

         // clear error website
        $("#website").change(function () {
            $("#er_website").html("");
        });

         // clear error logo
        $("#logo").change(function () {
            $("#er_logo").html("");
        });

    });

// show modal 
function showModal() {
    $("#er_name").html("");
    $("#er_email").html("");
    $("#er_website").html("");
    $("#er_logo").html(""); 

    $("#name").val('');
    $("#email").val('');
    $("#website").val('');
    $("#id_companies").val('');
    $("#logo").val('');
    $("#title_modal").html("Form Add Companies");
    $("#modal-companies").modal("show"); 
}

// get data edit
function setEditComapnies(name,email,website,logo,id_companies)
{
    $("#name").val(name);
    $("#email").val(email);
    $("#website").val(website);
    $("#id_companies").val(id_companies);
  //  $("#logo").val(logo);
    $("#title_modal").html("Form Edit Companies");
    $("#modal-companies").modal("show"); 
}

// get data delete
function setDeleteCompanies(id,name)
{
    $("#id_delete").val(id);
    $("#nama_company").html(name);
    $("#modal_delete").modal("show"); 
}

// delete data company
function deleteCompany()
{
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        type: "POST",
        url: "companies/delete-companies",
        cache: false,
        datetype: "JSON",
        data: {
            id_delete: $("#id_delete").val(),
        },
       
        success: function (data) {
            location.reload();
        },
    });
}

// show logo company
function logoCompany(id)
{

    var id_company = id;
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        type: "POST",
        url: "companies/show-logo",
        cache: false,
        datetype: "JSON",
        data: {
            id_company: id_company
        },
       
        success: function (data) {
            $("#data_logo").html(data);
            $("#modal_logo").modal('show');
        },
    });
}