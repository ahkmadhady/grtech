// save employee
function saveEmployee()
{
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        type: "POST",
        url: "employees/save-employees",
        cache: false,
        datetype: "JSON",
        data: {
            first_name: $("#first_name").val(),
            last_name: $("#last_name").val(),
            email: $("#email").val(),
            company: $("#company").val(),
            phone: $("#phone").val()
        },
       
        success: function (data) {
            if (data.errors) { 

                if (data.errors.first_name) {
                    $("#er_first_name").html(data.errors.first_name);
                }else{
                    $("#er_first_name").html("");
                }

                if (data.errors.last_name) {
                    $("#er_last_name").html(data.errors.last_name);
                }else{
                    $("#er_last_name").html("");
                }

                if (data.errors.email) {
                    $("#er_email").html(data.errors.email);
                }else{
                    $("#er_email").html("");
                }

                if (data.errors.company) {
                    $("#er_company").html(data.errors.company);
                }else{
                    $("#er_company").html("");
                }

                if (data.errors.phone) {
                    $("#er_phone").html(data.errors.phone);
                }else{
                    $("#er_phone").html("");
                } 

            } else {
            
            $("#er_phone").html("");
            $("#er_company").html("");
            $("#er_email").html("");
            $("#er_last_name").html("");
            $("#er_first_name").html(""); 
             location.reload(true);
            }
        },
    });
}

// update employee
function updateEmployee()
{
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        type: "POST",
        url: "employees/update-employees",
        cache: false,
        datetype: "JSON",
        data: {
            first_name: $("#first_name").val(),
            last_name: $("#last_name").val(),
            email: $("#email").val(),
            company: $("#company").val(),
            phone: $("#phone").val(),
            id_employee: $("#id_employee").val()
        },
       
        success: function (data) {
            if (data.errors) { 

                if (data.errors.first_name) {
                    $("#er_first_name").html(data.errors.first_name);
                }else{
                    $("#er_first_name").html("");
                }

                if (data.errors.last_name) {
                    $("#er_last_name").html(data.errors.last_name);
                }else{
                    $("#er_last_name").html("");
                }

                if (data.errors.email) {
                    $("#er_email").html(data.errors.email);
                }else{
                    $("#er_email").html("");
                }

                if (data.errors.company) {
                    $("#er_company").html(data.errors.company);
                }else{
                    $("#er_company").html("");
                }

                if (data.errors.phone) {
                    $("#er_phone").html(data.errors.phone);
                }else{
                    $("#er_phone").html("");
                } 

            } else {
            
            $("#er_phone").html("");
            $("#er_company").html("");
            $("#er_email").html("");
            $("#er_last_name").html("");
            $("#er_first_name").html(""); 
             location.reload(true);
            }
        },
    });
}

// action submit 
function actionSubmit()
{
    var id_employee = $("#id_employee").val();

    if (id_employee =="") {
        saveEmployee();
    }else{
        updateEmployee();
    }
}

 $(document).ready(function () {
        // clear error first name
        $("#first_name").change(function () {
             $("#er_first_name").html("");
        });

         // clear error last name
        $("#last_name").change(function () {
            $("#er_last_name").html("");
        });

         // clear error email
        $("#email").change(function () {
            $("#er_email").html("");
        });

         // clear error company
        $("#company").change(function () {
            $("#er_company").html("");
        });

          // clear error phone
        $("#phone").change(function () {
            $("#er_phone").html("");
        });

    });

// show modal 
function showModal() {
    $("#er_first_name").html("");
    $("#er_last_name").html("");
    $("#er_company").html("");
    $("#er_email").html("");
    $("#er_phone").html(""); 

    $("#first_name").val('');
    $("#id_employee").val('');
    $("#last_name").val('');
    $("#email").val('');
    $("#company").val('');
    $("#phone").val('');
    $("#title_modal").html("Form Add Employee");
    $("#modal_employee").modal("show"); 
}
 
// show data company
function showCompany(name)
{
    
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        type: "POST",
        url: "employees/data-company",
        cache: false,
        datetype: "JSON",
        data: {
            company: name
        },
       
        success: function (data) {
            $("#datas_company").html("");
            $("#datas_company").html(data);
            $("#modal_company").modal('show');
        },
    });
}

// get data edit 
function editEmployee(id,first_name,last_name,email,company,phone)
{
    $("#first_name").val(first_name);
    $("#id_employee").val(id);
    $("#last_name").val(last_name);
    $("#email").val(email);
    $("#company").val(company);
    $("#phone").val(phone);
    $("#title_modal").html("Form Edit Employee");
    $("#modal_employee").modal("show"); 
}

// get data delete
function setDeleteEmployee(id,name)
{
    $("#id_delete").val(id);
    $("#nama_employee").html(name);
    $("#modal_delete").modal("show"); 
}

// delete data employee
function deleteEmployee()
{
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        type: "POST",
        url: "employees/delete-employees",
        cache: false,
        datetype: "JSON",
        data: {
            id_delete: $("#id_delete").val()
        },
       
        success: function (data) {
             location.reload(true); 
        },
    });
}
