<?php

use App\Http\Controllers\CompaniesControllers;
use App\Http\Controllers\EmployeesControllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::group(['middleware' => ['auth','rule']], function(){

    // companies
    Route::get('companies', [CompaniesControllers::class, 'index'])->name('companies');
    Route::post('companies/save-companies', [CompaniesControllers::class, 'store']);
    Route::post('companies/delete-companies', [CompaniesControllers::class, 'destroy']);
    Route::post('companies/show-logo', [CompaniesControllers::class, 'showLogo']);

    // employee
    Route::get('employees', [EmployeesControllers::class, 'index'])->name('employees');
    Route::post('employees/save-employees', [EmployeesControllers::class, 'store']);
    Route::post('employees/update-employees', [EmployeesControllers::class, 'update']);
    Route::post('employees/delete-employees', [EmployeesControllers::class, 'destroy']);
    Route::post('employees/data-company', [EmployeesControllers::class, 'showCompany']);

});

require __DIR__.'/auth.php';
