<?php

namespace App\Http\Controllers;


use App\Models\Companies;
use App\Models\Employees;
use Illuminate\Http\Request;
use File;
use Response;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use DB;
class EmployeesControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $companies = Companies::select('name','id')->OrderBy('id','desc')->get();
        if ($request->ajax()) {
            $data = Employees::select(DB::raw('CONCAT(last_name, ", ", first_name) as full_name,email,company,phone,id,first_name,last_name'))->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('Action', function($row){ 
                           $btn = '<a href="javascript:void(0)"  class="edit btn btn-warning btn-sm" onclick="editEmployee(`'.$row->id.'`,`'.$row->first_name.'`,`'.$row->last_name.'`,`'.$row->email.'`,`'.$row->company.'`,`'.$row->phone.'`)">Edit</a>

                            <a href="javascript:void(0)" class="hapus btn btn-danger btn-sm" onclick="setDeleteEmployee(`'.$row->id.'`,`'.$row->first_name.'`)">Delete</a>         
                           ';
                            return $btn;
                    })
                    ->rawColumns(['Action'])
                    ->make(true);
        }

        $tesnama = Employees::all();
        
        return view('employees.employees',compact('companies','tesnama'));
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = array(
            'first_name'    => 'required|max:80', 
            'last_name'     => 'required|max:80', 
            'email'         => 'email|max:35|unique:employees',
            'company'       => 'max:90',
            'phone'         => 'max:15'
        ); 

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return Response::Json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        
        $employees = Employees::create([
            'first_name'   => $request->first_name,
            'last_name'    => $request->last_name,
            'company'      => $request->company,
            'email'        => $request->email,
            'phone'        => $request->phone
        ]);
 
        if ($employees) { 
            return response()->json([
                'message' => 'employees successfully saved',
                'status'  => 'success'
            ], 200);
        } else {
 
            return response()->json([
                'message' => 'Something went wrong',
                'status'  => 'error'
            ], 500);
        }
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validation = array(
            'first_name'    => 'required|max:80', 
            'last_name'     => 'required|max:80', 
            'email'         => 'email|max:35|unique:employees,email,'. $request->id_employee,
            'company'       => 'max:90',
            'phone'         => 'max:15'
        ); 

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return Response::Json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        
        $employees = Employees::where('id',$request->id_employee)->update([
            'first_name'   => $request->first_name,
            'last_name'    => $request->last_name,
            'company'      => $request->company,
            'email'        => $request->email,
            'phone'        => $request->phone
        ]);

        if ($employees) { 
            return response()->json([
                'message' => 'employees successfully saved',
                'status'  => 'success'
            ], 200);
        } else {
 
            return response()->json([
                'message' => 'Something went wrong',
                'status'  => 'error'
            ], 500);
        }
    }

    // show data company
    public function showCompany(Request $request)
    {
        $companies = Companies::select('name','email','website','logo')->where('name',$request->company)->first();
        return view('employees.data_companies',compact('companies'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $employees = Employees::where('id',$request->id_delete)->delete();
        if ($employees) { 
            return response()->json([
                'message' => 'employees successfully deleted',
                'status'  => 'success'
            ], 200);
        } else {
 
            return response()->json([
                'message' => 'Something went wrong',
                'status'  => 'error'
            ], 500);
        }
    }
}
