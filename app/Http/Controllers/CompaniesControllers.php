<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use App\Models\Employees;
use Illuminate\Http\Request;
use File;
use Response;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
class CompaniesControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Companies::all();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('Action', function($row){ 
                         
                           $btn = '<a href="javascript:void(0)" class="edit btn btn-warning btn-sm" onclick="setEditComapnies(`'.$row->name.'`,`'.$row->email.'`,`'.$row->website.'`,`'.$row->logo.'`,`'.$row->id.'`)">Edit</a>

                            <a href="javascript:void(0)" class="hapus btn btn-danger btn-sm" onclick="setDeleteCompanies(`'.$row->id.'`,`'.$row->name.'`)">Delete</a>         
                           ';
                            return $btn;
                    })
                    ->rawColumns(['Action'])
                    ->make(true);
        } 
        return view('companies.companies');
    }
  
    // save & update data companies

    public function store(Request $request)
    { 
        
        $id_companies = $request->id_companies;
         
        if ($id_companies =="") {
 
            $validation = array(
                'name'       => 'required|max:55|unique:companies', 
                'email'      => 'email|max:35|unique:companies',
                'website'    => 'max:55',
                'logo'       => 'mimes:jpeg,png,jpg', 
            ); 

            $validator = Validator::make($request->all(), $validation);

            if ($validator->fails()) {
                return Response::Json(array('errors' => $validator->getMessageBag()->toArray()));
            }

            $logo    =  $request->file('logo');
            if ($logo != '') {
                // directory
                $directory = 'logo';
                // company name
                $company = str_replace(' ', '_', $logo->getClientOriginalName()); 
                // upload file
                $logo->move($directory,date('Ymdhi')."".$company);
                $file_logo = date('Ymdhi')."".$company; 
                
            } else {
                $file_logo  = '';
            }

            $companies = Companies::create([
                'name'      => $request->name,
                'email'     => $request->email,
                'website'   => $request->website,
                'logo'      => $file_logo
            ]);
    
            if ($companies) { 
                return response()->json([
                    'message' => 'companies successfully saved',
                    'status'  => 'success'
                ], 200);
            } else {
     
                return response()->json([
                    'message' => 'Something went wrong',
                    'status'  => 'error'
                ], 500);
            }

        }else{

            $validation = array(
                'name'       => 'required|max:55', 
                'email'      => 'email|max:35|unique:companies,email,'. $request->id_companies,
                'website'    => 'max:55',
                'logo'       => 'mimes:jpeg,png,jpg', 
            ); 

            $validator = Validator::make($request->all(), $validation);

            if ($validator->fails()) {
                return Response::Json(array('errors' => $validator->getMessageBag()->toArray()));
            }

            $logo    =  $request->file('logo');
            if ($logo != '') {
                // directory
                $directory = 'logo';
                // company name
                $company = str_replace(' ', '_', $logo->getClientOriginalName()); 
                // upload file
                $logo->move($directory,date('Ymdhi')."".$company);
                $file_logo = date('Ymdhi')."".$company; 
                
            } else {
                $file_logo  = '';
            }

            // update companies
            $companies = Companies::where('id',$request->id_companies)->update([
                'name'      => $request->name,
                'email'     => $request->email,
                'website'   => $request->website,
                'logo'      => $file_logo
            ]);
    
            if ($companies) { 
                return response()->json([
                    'message' => 'companies successfully updated',
                    'status'  => 'success'
                ], 200);
            } else {
     
                return response()->json([
                    'message' => 'Something went wrong',
                    'status'  => 'error'
                ], 500);
            }
        }
    }

    // show logo company
    public function showLogo(Request $request)
    {
        $logo = Companies::find($request->id_company);
        return view('companies.data_logo',compact('logo'));
    }  
   
    public function destroy(Request $request)
    {
        $companies = Companies::where('id',$request->id_delete)->delete();
        if ($companies) { 
            return response()->json([
                'message' => 'companies successfully deleted',
                'status'  => 'success'
            ], 200);
        } else {
 
            return response()->json([
                'message' => 'Something went wrong',
                'status'  => 'error'
            ], 500);
        }
    }
}
